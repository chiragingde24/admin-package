<?php
namespace Cs\AdminPanel\Controllers;

use Illuminate\Http\Request;
use Cs\AdminPanel\Chirag;

class AdminPanelController
{
    public function __invoke(Chirag $chirag) {
        $quote = $chirag->justDoIt();
        return view('chirag::index', compact('quote'));
    }
}