<?php

use Cs\AdminPanel\Controllers\AdminPanelController;
use Cs\AdminPanel\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('weather', AdminPanelController::class);